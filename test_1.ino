// Incluímos la librería para poder controlar el servo
#include <Servo.h>
 
// Declaramos la variable para controlar el servo
Servo ServoMotor;
// Definimos un código para indicarle acciones desde la consola
char CodigoSerie;

int SERVOARRIBA=180; //Posición servo para no pintar
int SERVOABAJO=170;  //Posición servo para pintar

void setup() 
  {
  // Inicializamos la comunicación serie
  Serial.begin(9600);
  CodigoSerie = (char)(('0'));
 
  // Iniciamos el servo para que empiece a trabajar con el pin 3
  ServoMotor.attach(3);
  ServoMotor.write(SERVOARRIBA);
  }
 
void loop() 
  {
  if (Serial.available())
    {
    CodigoSerie = (Serial.read());
    if (CodigoSerie == ('S'))
      {
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);
      }
    if (CodigoSerie == ('s'))
      {
      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
      }
    }
  }
