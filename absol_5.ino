// Incluímos la librería para poder controlar el servo
#include <Servo.h>

// Librería para controla la aceleración del motor paso a paso
#include <AccelStepper.h>

// Declaramos la variable para controlar el servo
Servo ServoMotor;

// Definimos un código para indicarle acciones desde la consola
char CodigoSerie;

int PINDIRX=10;       //Define Pin Pulse
int PINPULX=11;       //Define Pin Direction
int SERVOARRIBA=180;  //Posición servo para no pintar
int SERVOABAJO=170;   //Posición servo para pintar
long PULSOSX=0;        //Pulsos a enviar al Motor PAP del eje X
int TIEMPO=1000;      //Tiempo en microsegundos entre pulsos
long PULSOSY=0;
int PINPULY = 6;
int PINDIRY = 5;
int PUL_POR_CENTIMIL = 4; //Pulsos por centesimas de milimetro

// Crea una nueva instancia de la clase AccelStepper
AccelStepper MOTORPAPX(AccelStepper::DRIVER, PINPULX, PINDIRX);
AccelStepper MOTORPAPY(AccelStepper::DRIVER, PINPULY, PINDIRY);


void setup() {
  // Inicializamos la comunicación serie
  Serial.begin(9600);
  CodigoSerie = (char)(('0'));

  // Iniciamos el servo para que empiece a trabajar con el pin 3
  ServoMotor.attach(3);
  ServoMotor.write(SERVOARRIBA);

  // Ajustamos la velocidad y aceleración máxima y la velocidad actual
  MOTORPAPX.setAcceleration(1000);
  MOTORPAPX.setMaxSpeed(1000);
  MOTORPAPX.setSpeed(100);

  MOTORPAPY.setAcceleration(1000);
  MOTORPAPY.setMaxSpeed(1000);
  MOTORPAPY.setSpeed(100);
}
 
void loop() {
  //Serial.println(MOTORPAPX.targetPosition());
  if (Serial.available()) {
    CodigoSerie = (Serial.read());

    // Mueve en el eje X los pulsos indicados      
    if (CodigoSerie == ('x')) {
      
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);

      // Lee el valor numérico y elije dirección 
      PULSOSX=(Serial.readString()).toInt()*PUL_POR_CENTIMIL;

      // Ajusta la posición de destino
      MOTORPAPX.move(PULSOSX);
      // Se mueve a la posición de destino con velocidad y aceleración/deceleración
      MOTORPAPX.runToPosition();

      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
    }

    if (CodigoSerie == ('X')) {
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);
      // Lee el valor numérico y elije dirección 
      PULSOSX=(Serial.readString()).toInt()*PUL_POR_CENTIMIL;
      Serial.println(PULSOSX);
      // Ajusta la posición de destino
      MOTORPAPX.moveTo(PULSOSX);
      // Se mueve a la posición de destino con velocidad y aceleración/deceleración
      MOTORPAPX.runToPosition();
      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
    }

    if (CodigoSerie == ('Y')) {
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);
      // Lee el valor numérico y elije dirección 
      PULSOSY=(Serial.readString()).toInt()*PUL_POR_CENTIMIL;
      // Ajusta la posición de destino
      MOTORPAPY.moveTo(PULSOSY);
      // Se mueve a la posición de destino con velocidad y aceleración/deceleración
      MOTORPAPY.runToPosition();
      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
    }
    if (CodigoSerie == 'P') {
      testPunto();
    }
  }
}

void testPunto() {
    MOTORPAPX.runToNewPosition(0); 

    MOTORPAPX.runToNewPosition(-5000); 

    MOTORPAPX.runToNewPosition(-1000); 

    MOTORPAPX.runToNewPosition(-1200); 
} 
