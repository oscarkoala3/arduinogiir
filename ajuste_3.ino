// Incluímos la librería para poder controlar el servo
#include <Servo.h>

// Declaramos la variable para controlar el servo
Servo ServoMotor;
// Definimos un código para indicarle acciones desde la consola
char CodigoSerie;

int PINENAX=9;        //Define Pin Enable 
int PINDIRX=10;       //Define Pin Direction
int PINPULX=11;       //Define Pin Pulse
int PINENAY=4;        //Define Pin Enable 
int PINDIRY=5;       //Define Pin Direction
int PINPULY=6;       //Define Pin Pulse
int SERVOARRIBA=180;  //Posición servo para no pintar
int SERVOABAJO=138;   //Posición servo para pintar
int PULSOSX=0;        //Pulsos a enviar al Motor PAP del eje X
int PULSOSY=0; 
int TIEMPO=175;           //Tiempo en microsegundos entre pulsos
int PULSO_POR_CENTMIL = 4;
//14.3mm/s

void setup() {
  // Inicializamos la comunicación serie
  Serial.begin(9600);
  CodigoSerie = (char)(('0'));

  // Se configuran los pines conectados al driver TB6600 como salidas
  pinMode (PINPULX, OUTPUT);
  pinMode (PINDIRX, OUTPUT);
  pinMode (PINENAX, OUTPUT);
  pinMode (PINPULY, OUTPUT);
  pinMode (PINDIRY, OUTPUT);
  pinMode (PINENAY, OUTPUT);
 
  // Iniciamos el servo para que empiece a trabajar con el pin 3
  ServoMotor.attach(3);
  ServoMotor.write(SERVOARRIBA);

  // Habilita el controlador para que el motor funcione
  digitalWrite(PINENAX,HIGH); 
  digitalWrite(PINENAY,HIGH);
}
 
void loop() {
  if (Serial.available()) {
    CodigoSerie = (Serial.read());
    // Mueve en el eje X los pulsos indicados      
    if (CodigoSerie == ('x')) {
      
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);

      // Lee el valor numérico y elije dirección 
      PULSOSX=(Serial.readString()).toInt();
      if (PULSOSX > 0) {
        // Activa dirección de avance
        digitalWrite(PINDIRX,HIGH);         
      }
      else {
        // Activa dirección de retroceso
        digitalWrite(PINDIRX,LOW);
        PULSOSX=abs(PULSOSX);
      }
      PULSOSX*=PULSO_POR_CENTMIL;
      //Envia PULSOSX pulsos
      for (int i=0; i<PULSOSX; i++) {
        digitalWrite(PINPULX,HIGH);
        delayMicroseconds(TIEMPO);
        digitalWrite(PINPULX,LOW);
        delayMicroseconds(TIEMPO);
      }

      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
    }

    if (CodigoSerie == ('y')) {
      
      // El servo mueve la cremallera a la posición inferior
      ServoMotor.write(SERVOABAJO);

      // Lee el valor numérico y elije dirección 
      PULSOSY=(Serial.readString()).toInt();
      if (PULSOSY > 0) {
        // Activa dirección de avance
        digitalWrite(PINDIRY,HIGH);         
      }
      else {
        // Activa dirección de retroceso
        digitalWrite(PINDIRY,LOW);
        PULSOSY=abs(PULSOSY);
      }
      PULSOSY*=PULSO_POR_CENTMIL;
      //Envia PULSOSX pulsos
      for (int i=0; i<PULSOSY; i++) {
        digitalWrite(PINPULY,HIGH);
        delayMicroseconds(TIEMPO);
        digitalWrite(PINPULY,LOW);
        delayMicroseconds(TI ,EMPO);
      }

      // El servo mueve la cremallera a la posición superior
      ServoMotor.write(SERVOARRIBA);
    }
  }
}
