// MultiStepper.pde
// -*- mode: C++ -*-
// Use MultiStepper class to manage multiple steppers and make them all move to 
// the same position at the same time for linear 2d (or 3d) motion.
#include <Servo.h> 
#include <AccelStepper.h>
#include <MultiStepper.h>


#define PINDIRX 10
#define PINPULX 11
#define PINDIRY 5
#define PINPULY 6

long positions[2]; // Array of desired stepper positions
int SERVOARRIBA=180;  //Posición servo para no pintar
int SERVOABAJO=150;   //Posición servo para pintar
// EG X-Y position bed driven by 2 steppers
// Alas its not possible to build an array of these with different pins for each :-(
AccelStepper MOTORPAPX(AccelStepper::DRIVER, PINPULX, PINDIRX);
AccelStepper MOTORPAPY(AccelStepper::DRIVER, PINPULY, PINDIRY);
 
// Up to 10 steppers can be handled as a group by MultiStepper
MultiStepper ROBOTXY;
Servo ServoMotor;
char CodigoSerie;
 
void setup() {
  Serial.begin(9600);
  ServoMotor.attach(3);
  CodigoSerie = (char)(('0'));
 
  // Configure each stepper
  MOTORPAPX.setSpeed(500);
  MOTORPAPX.setMaxSpeed(1000);

  MOTORPAPY.setSpeed(500);
  MOTORPAPY.setMaxSpeed(1000);
 
  // Then give them to MultiStepper to manage
  ROBOTXY.addStepper(MOTORPAPX);
  ROBOTXY.addStepper(MOTORPAPY);
  Escribiendo(false);
}

void Escribiendo(bool Escribiendo){
  if (Escribiendo){
    ServoMotor.write(SERVOABAJO);
  }else{
    ServoMotor.write(SERVOARRIBA);    
  }
}
  
 
void loop() {

  if (Serial.available())  {
    CodigoSerie = Serial.read();
    if (CodigoSerie == 'T') {
      dibujitoUwu();
    }
    if (CodigoSerie == 'C') {
      cuadradoUwu();
    }
    if (CodigoSerie == 'O') {
      irA(-10000,0,false);
    }
  }
  
  /*Escribiendo(true);
  positions[0] = 0;
  positions[1] = 0;
  ROBOTXY.moveTo(positions);
  ROBOTXY.runSpeedToPosition(); // Blocks until all are in position
  delay(1000);
  Escribiendo(false);
  
  // Move to a different coordinate
  positions[0] = 2000;
  positions[1] = 2000;
  ROBOTXY.moveTo(positions);
  ROBOTXY.runSpeedToPosition(); // Blocks until all are in position
  delay(1000);*/
  
}

void irA(long x, long y,bool escribiendo) {
  Escribiendo(escribiendo);
  positions[0] = x;
  positions[1] = y;
  ROBOTXY.moveTo(positions);
  ROBOTXY.runSpeedToPosition();
  delay(500);
}

void dibujitoUwu() {
  irA(2440,2440,true);
  irA(4880,0,true);
  irA(0,0,true);
  Escribiendo(false);
}
void cuadradoUwu() {
  irA(2000,0,true);
  irA(2000,2000,true);
  irA(0,2000,true);
  irA(0,0,true);
  Escribiendo(false);
}